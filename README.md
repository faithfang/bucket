A prototype project for viewing data in list and detail structure.

Add data into a bucket.

Update item amount from the list, detail and bucket.

Sync the amount among the list, detail and bucket.

Maintain a local cache.