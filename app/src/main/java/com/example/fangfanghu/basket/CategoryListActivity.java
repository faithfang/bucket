package com.example.fangfanghu.basket;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class CategoryListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ListView listView = (ListView) findViewById(R.id.categories_listView);
        String[] values = new String[]{"Category 1", "Category 2"};

        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent productListIntent = new Intent(CategoryListActivity.this, ProductListActivity.class);
                startActivity(productListIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((Button)findViewById(R.id.button_basket)).setText(String.valueOf(Bucket.getInstance().getTotalProductAmount()));
    }
}
