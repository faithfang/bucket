package com.example.fangfanghu.basket;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fangfanghu on 28/04/15.
 */
public class ProductListBaseAdapter extends BaseAdapter {
    private Context mContext;
    private List<Product> products = new ArrayList<Product>();
    private AmountChangedListener mAmountChangedListener;
    public ProductListBaseAdapter(Context context, List<Product> products, AmountChangedListener amountChangedListener){
        mContext = context;
        this.products = products;
        this.mAmountChangedListener = amountChangedListener;
    }

    public interface AmountChangedListener {
        public void amountChanged(Product product);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Product getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(products.get(position).getId());
    }

    private class ViewHolder {
        TextView nameView;
        TextView priceView;
        TextView amountView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_layout, null);
            holder = new ViewHolder();
            holder.nameView = (TextView) convertView.findViewById(R.id.name);
            holder.priceView = (TextView) convertView.findViewById(R.id.price);
            holder.amountView = (TextView) convertView.findViewById(R.id.amount);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Product product = products.get(position);

        holder.nameView.setText(product.getName());
        holder.priceView.setText(product.getPrice());
        holder.amountView.setText(String.valueOf(product.getAmount()));


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.amount_dialog);

                // set the custom dialog components - text, image and button
                final EditText text = (EditText) dialog.findViewById(R.id.edit_amount);

                Button dialogButton = (Button) dialog.findViewById(R.id.button_ok);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        product.setAmount(Integer.valueOf(text.getText().toString()));
                        mAmountChangedListener.amountChanged(product);
                    }
                });

                dialog.show();
            }
        });

        return convertView;
    }

}
