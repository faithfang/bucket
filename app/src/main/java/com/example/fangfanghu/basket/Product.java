package com.example.fangfanghu.basket;

/**
 * Created by fangfanghu on 28/04/15.
 */
public class Product {
    private String id;
    private String name;
    private String price;
    private int amount;

    public Product(){

    }

    public void setId(String id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(String price){
        this.price = price;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getPrice(){
        return price;
    }

    public int getAmount(){
        return amount;
    }

    public Product getClone(){
        Product product = new Product();
        product.setAmount(amount);
        product.setPrice(price);
        product.setName(name);
        product.setId(id);
        return product;
    }
}
