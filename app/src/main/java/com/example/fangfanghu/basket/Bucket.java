package com.example.fangfanghu.basket;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by fangfanghu on 28/04/15.
 */
public class Bucket {
    private static Bucket bucket = new Bucket();
    private List<Product> checkOutList = new ArrayList<Product>();

    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private Bucket(){ }

    /* Static 'instance' method */
    public static Bucket getInstance( ) {
        return bucket;
    }

    public List<Product> getCheckOutList(){
        return checkOutList;
    }

    public int getTotalProductAmount(){
        if(checkOutList.size()==0){
            return 0;
        }else {
            int total = 0;
            for(Product product:checkOutList){
                total+=product.getAmount();
            }
            return total;
        }
    }

    public void addProduct(Product product1){
        if(product1.getAmount()==0){
            removeProduct(product1);
        }else{
            boolean added = false;
            for(Product product: checkOutList){
                if(product.getId().equals(product1.getId())){
                    product.setAmount(product1.getAmount());
                    added = true;
                    break;
                }
            }
            if(!added){

                checkOutList.add(product1);
            }
        }
    }

    public void removeProduct(Product product){
        Iterator<Product> iterator = checkOutList.iterator();
        if(iterator.hasNext()){
            if(iterator.next().getId()==product.getId()){
                iterator.remove();
            }
        }
    }
}
