package com.example.fangfanghu.basket;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fangfanghu on 28/04/15.
 */
public class BucketActivity extends Activity implements ProductListBaseAdapter.AmountChangedListener{
    private ProductListBaseAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        ListView listView = (ListView) findViewById(R.id.product_listView);

        adapter = new ProductListBaseAdapter(BucketActivity.this, Bucket.getInstance().getCheckOutList(), BucketActivity.this);

        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //update list per bucket
    }

    @Override
    public void amountChanged(Product product1) {
        if(product1.getAmount()==0){
            Bucket.getInstance().removeProduct(product1);
        }

        for(Product product: ProductCache.getInstance().getCheckOutList()){
            if(product.getId().equals(product1.getId())){
                product.setAmount(product1.getAmount());
                break;
            }
        }

        adapter.notifyDataSetInvalidated();
    }
}
