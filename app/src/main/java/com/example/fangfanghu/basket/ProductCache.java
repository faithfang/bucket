package com.example.fangfanghu.basket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fangfanghu on 29/04/15.
 */
public class ProductCache {
    private static ProductCache cache = new ProductCache();
    private List<Product> checkOutList = new ArrayList<Product>();

    private ProductCache(){
        Product product1 = new Product();
        product1.setId("1");
        product1.setName("Apple");
        product1.setPrice("123.00£");

        Product product2 = new Product();
        product2.setId("2");
        product2.setName("Orange");
        product2.setPrice("223.00£");

        checkOutList.add(product1);
        checkOutList.add(product2);
    }

    /* Static 'instance' method */
    public static ProductCache getInstance( ) {
        return cache;
    }

    public List<Product> getCheckOutList(){
        return checkOutList;
    }
}
