package com.example.fangfanghu.basket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fangfanghu on 28/04/15.
 */
public class ProductListActivity extends Activity implements ProductListBaseAdapter.AmountChangedListener{
    private ProductListBaseAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        ListView listView = (ListView) findViewById(R.id.product_listView);

        adapter = new ProductListBaseAdapter(ProductListActivity.this, ProductCache.getInstance().getCheckOutList(), ProductListActivity.this);

        listView.setAdapter(adapter);

        ((Button)findViewById(R.id.button_basket)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductListActivity.this, BucketActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //update list per bucket
        adapter.notifyDataSetInvalidated();
        ((Button)findViewById(R.id.button_basket)).setText(String.valueOf(Bucket.getInstance().getTotalProductAmount()));
    }

    @Override
    public void amountChanged(Product product) {
        Bucket.getInstance().addProduct(product.getClone());
        ((Button)findViewById(R.id.button_basket)).setText(String.valueOf(Bucket.getInstance().getTotalProductAmount()));
        adapter.notifyDataSetInvalidated();
    }
}
